import RequestBuilder from './RequestBuilder';
import Response from './Response';
import Request from './Request';

export default class BaseApi {
  _requestBuilder = null;
  _requests = new Map();

  constructor() {
    this.init();
  }

  init() {
    this.initRequestBuilder();
  }

  initRequestBuilder() {
    this._requestBuilder = this.makeRequestBuilder();
    return this;
  }

  makeRequestBuilder() {
    return new RequestBuilder();
  }

  getRequestBuilder() {
    return this._requestBuilder;
  }

  pushRequest(request) {
    this._requests.set(request.getID(), request);
    return this;
  }

  removeRequest(request) {
    this._requests.delete(request.getID());
    return this;
  }

  removeRequestById(id) {
    this._requests.delete(id);
    return this;
  }

  cancelRequestById(id) {
    let request = this._requests.get(id);
    request.cancel();

    this.removeRequestById(id);
    return this;
  }

  clearAllRequests() {
    this._requests.forEach((val, key, map) => {
      val.cancel();
    });

    this._requests.clear();
  }

  withCancelToken(config, cancelToken) {
    return Object.assign({}, config, {cancelToken});
  }

  all(requests, callback) {
    return this.getRequestBuilder().multi(requests, callback);
  }

  request(method, url, data = {}, headers = {}, config = {}, id = undefined) {
    // создаем токен отмены запроса
    let requestBuilder = this.getRequestBuilder();
    let cancelSource = requestBuilder.buildCancelSource();

    // отправляем сам запрос
    let promise = requestBuilder[method](url, data, headers, this.withCancelToken(config, cancelSource.token));
    // оборачиваем запрос в класс, для удобного использования
    let request = (new Request(promise, id))
      .setCancelSource(cancelSource)
    ;

    //пушим запрос в список активных
    this.pushRequest(request);

    return request;
  }

  // для отправки запроса мимо реализации Api
  raw(callback) {
    return callback(this.getRequestBuilder());
  }

  // для отправки запроса, игнорируюя middleware
  force(...args) {
    return new Promise((resolve, reject) => {
      let request = this.request(...args);
      request.get()
        .then(response => resolve(response))
        .catch(error => reject(error))
      ;
    });
  }

  // обычная отправка запроса
  send(...args) {
    return this.middleware(() => this.request(...args));
  }

  middleware(buildRequest) {
    return new Promise((resolve, reject) => {
      let request = buildRequest();
      let validResponse = true;

      // обрабатываем результат
      request.get()
        .then(response => {
          // console.log(`====${response.status}: ${response.config.url}`);

          // запрос прошел успешно
          this.removeRequest(request);
          if (this.validateResponse(response)) {
            resolve(Response.fromResponse(response));
          } else {
            validResponse = false;
            throw this.getRequestBuilder().createError('', response.config, '', response.request, response);
          }
        })
        .catch(error => {

          // let errorText = "";
          // if (error.response) {
          //     errorText = `${error.response.status}: ${error.response.config.url}`;
          // } else if (error.request) {
          //     errorText = `???: ${error.request._url}`;
          // }
          // console.log("====" + errorText);

          //возникла ошибка
          this.removeRequest(request);

          if (this.getRequestBuilder().isCancel(error)) {
            reject(Response.cancelled());
          } else if (error.response) {
            let response = error.response;
            let status = response.status;

            if (validResponse) {
              this.handleErrorStatus(status, Response.fromResponse(response));
              reject(Response.httpError(response));
            } else {
              reject(Response.invalidResponse(response));
            }
          } else if (error.request) {
            reject(Response.noResponse());
          } else {
            reject(Response.undefinedError());
          }
        });
    });
  }

  makeResponse(response, reason) {
    return new Response(response, reason);
  }

  handleErrorStatus(status, data=null) {
    switch (status) {
      case 401:
        this.clearAllRequests();
        break;
    }
  }

  validateResponse(response) {
    return true;
  }
}

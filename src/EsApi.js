import BaseApi from './BaseApi';
import RequestBuilder from './RequestBuilder';

class EsApi extends BaseApi {
    _token = null;
    _tokenType = null;
    _httpHandlers = new Map();
    _config = {};

    setToken(tokenType, token) {
        this._tokenType = tokenType;
        this._token = token;
        this.initRequestBuilder();
    }

    setHttpResponseHandler(httpCode, callback) {
        this._httpHandlers.set(httpCode, callback);
        return this;
    }

    configure(config) {
        this._config = config;
        this.initRequestBuilder();

        return this;
    }

    /**
     * @override
     */
    makeRequestBuilder() {
        let commonHeaders = {
            'Authorization': `${this._tokenType} ${this._token}`,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        };

        let config = this._config || {};
        let {headers, ...otherConfigProps} = config;

        if (headers && headers.common) {
            commonHeaders = Object.assign(commonHeaders, headers.common);
        }

        return new RequestBuilder({
            headers: {
                common: commonHeaders,
            },
            timeout: 60 * 1000,
            validateStatus: (status) => status >= 200 && status < 300,
            ...otherConfigProps,
        });
    }

    /**
     * @override
     */
    validateResponse(response) {
        return response.data.error === 0;
    }

    /**
     * @override
     */
    handleErrorStatus(status, data = null) {
        if (this._httpHandlers.has(status)) {
            return this._httpHandlers.get(status)(this, data);
        }
    }

    /**
     * ================================================================
     * API
     * ================================================================
     */

    getMainData() {
        return this.send('get', '/main/data');
    }

    getMainMobileData() {
        return this.send('get', '/main/mobile');
    }

    getObjectsList() {
        return this.send('get', '/object/list');
    }

    getBranchesList() {
        return this.send('get', '/branches');
    }

    getAccountBalance(account_id) {
        return this.send('get', `/account/balance?account_id=${account_id}`);
    }

    getAccountAccruals(account_id) {
        return this.send('get', `/account/accruals?account_id=${account_id}`);
    }

    getReceipt(account_id, period) {
        return this.send('get', '/receipt/download', {
            account_id,
            period,
        });
    }

    getReport(doc_type, doc_id, doc_name = null) {
        return this.send('get', '/entity/report/download', {
            doc_type,
            doc_id,
            doc_name
        });
    }

    addAccount(data) {
        return this.send('post', '/account/connect', data);
    }

    getPromotionsList(branchCode) {
        return this.send('get', `/promotions/list?branch_code=${branchCode}`);
    }

    getMeterDeviceList(accountId) {
        return this.send('get', `/meter/list?account_id=${accountId}`);
    }

    getObjectMeterDeviceList(accountId) {
        return this.send('get', `/meter/object/list?account_id=${accountId}`);
    }

    getNewNotifications(timestamp, requestId = undefined) {
        return this.send('get', '/notifications/new', {
            timestamp,
        }, {}, {}, requestId);
    }

    getNewUserPopup(requestId = undefined) {
        return this.send('get', '/notifications/popup', {}, {}, {}, requestId);
    }

    sendMeterDeviceMetrics(accountId, meterId, metrics) {
        return this.send('post', '/meter/data/send', {
            account_id: accountId,
            meter_id: meterId,
            t1: metrics.t1,
            t2: metrics.t2,
            t3: metrics.t3,
        });
    }

    setMeterRoomName(meter_id, name) {
        return this.send('post', '/meter/room/update', {
            meter_id,
            name,
        });
    }

    profile() {
        return this.send('get', '/settings/profile');
    }

    saveProfile(profile) {
        return this.send('post', '/settings/profile', profile);
    }

    socialConnect(social) {
        return this.send('post', '/settings/social/connect', {social});
    }

    socialConnectWithAccessToken(social, code, os) {
        return this.send('post', '/settings/social/connect/token', {social, code, os});
    }

    socialDisconnect(social) {
        return this.send('post', '/settings/social/disconnect', {social});
    }

    fetchAccounts() {
        return this.send('get', '/settings/accounts');
    }

    fetchSubscriptions() {
        return this.send('get', '/settings/subscriptions');
    }

    saveSubscription(account_id, subscription) {
        return this.send('post', '/settings/subscriptions', {account_id, ...subscription});
    }

    fetchEntitySubscriptions() {
        return this.send('get', '/entity/settings/subscriptions/list');
    }

    updateEntitySubscription(organization_id, subscriptions_type, contact_id) {
        return this.send('post', '/entity/settings/subscriptions/update', {
            organization_id,
            subscriptions_type,
            contact_id,
        });
    }

    fetchMeterDevices() {
        return this.send('get', '/settings/meters');
    }

    fetchObjects() {
        return this.send('get', '/settings/objects');
    }

    fetchServices() {
        return this.send('get', '/settings/services');
    }

    changeDeliveryMethodOfReceipt(account_id, delivery_type) {
        return this.send('post', '/account/receipt', {account_id, delivery_type});
    }

    initPaymentByAmount(payment_method, account_id, branch_code, amount, use_card, email) {
        return this.send('post', '/payment/init/amount', {
            payment_method,
            account_id,
            branch_code,
            amount,
            use_card,
            email,
        });
    }

    initPaymentByServices(payment_method, account_id, branch_code, services, use_card, email) {
        return this.send('post', '/payment/init/service', {
            payment_method,
            account_id,
            branch_code,
            services,
            use_card,
            email,
        });
    }

    initEntityPayment(payment_method, account_id, organization_id, branch_code, amount, description, fin_docs, use_card) {
        return this.send('post', '/entity/payment/init/amount', {
            payment_method,
            account_id,
            organization_id,
            branch_code,
            amount,
            description,
            fin_docs,
            use_card
        });
    }

    getPaymentStatus(transaction_id) {
        return this.send('get', '/payment/status', {
            transaction_id,
        });
    }

    connectAutoPayment(account_id) {
        return this.send('post', '/payment/card/register', {
            account_id,
        });
    }
    
    connectAutoPaymentEntity(account_id, organization_id, type, amount) {
        return this.send('post', '/payment/card/register-entity', {
            account_id, organization_id, type, amount
        });
    }

    updateAutoPaymentEntity(account_id, organization_id, type, amount) {
        return this.send('post', '/payment/card/update-entity', {
            account_id, organization_id, type, amount
        });
    }

    disconnectAutoPayment(account_id) {
        return this.send('post', '/payment/card/delete', {
            account_id,
        });
    }

    getAutoPaymentStatus(account_id) {
        return this.send('get', '/payment/card/status', {
            account_id,
        });
    }

    acceptApplePayOffer(transaction_id, payment_data) {
        return this.send('post', '/payment/offer/apple-pay', {
            transaction_id,
            payment_data,
        });
    }

    setUserDeviceToken(device_id, token = null, os) {
        return this.send('post', '/device', {
            device_id,
            token,
            os,
        });
    }

    markNotificationsViewed() {
        return this.send('post', '/notifications/view');
    }

    checkAppVersion(platform, version) {
        return this.send('get', '/mobile-app/updates/check', {
            platform,
            version,
        });
    }

    getFaqList(branch_code) {
        return this.send('get', '/faq/list', {
            branch_code,
        });
    }

    getOfficeList(branch_code) {
        return this.send('get', '/office/list', {
            branch_code,
        });
    }

    /**
     * ================================================================
     * Statistics
     * ================================================================
     */

    getPaymentsStatistics(account_id, period_from, period_to, limit, offset) {
        return this.send('get', '/statistics/payments', {
            account_id,
            period_from,
            period_to,
            limit,
            offset,
        });
    }

    getAccrualsStatistics(account_id, period_from, period_to, limit, offset, service_id) {
        return this.send('get', '/statistics/accruals', {
            account_id,
            period_from,
            period_to,
            limit,
            offset,
            service_id,
        });
    }

    getMetersStatistics(account_id, period_from, period_to, limit, offset, meter_ids) {
        return this.send('get', '/statistics/meters', {
            account_id,
            period_from,
            period_to,
            limit,
            offset,
            meter_ids,
        });
    }

    getConsumptionFilter(account_id) {
        return this.send('get', '/statistics/consumption/filter', {
            account_id,
        });
    }

    getConsumptionStatistics(account_id, period_from, period_to, limit, offset, meter_id) {
        return this.send('get', '/statistics/consumption', {
            account_id,
            period_from,
            period_to,
            limit,
            offset,
            meter_id,
        });
    }

    getEntityConsumptions(organization_id, account_id, premise_id, service_id, period_from, period_to, limit, offset) {
        return this.send('get', '/entity/statistics/consumptions', {
            organization_id,
            account_id,
            premise_id,
            service_id,
            period_from,
            period_to,
            limit,
            offset
        });

    }

    getProviders(organization_id, contract_id = null) {
        return this.send('get', '/entity/settings/providers/list', {
            organization_id,
            contract_id
        });
    }

    getReferences(system_id, reference_name) {
        return this.send('get', '/references/list', {
            system_id,
            reference_name
        });
    }

    deleteAccount(account_id) {
        return this.send('post', '/account/disconnect', {
            account_id,
        });
    }

    /**
     * ================================================================
     * Contacts
     * ================================================================
     */

    loadContacts() {
        return this.send('get', '/settings/contacts/list');
    }

    addContact(type, contact) {
        return this.send('post', '/settings/contacts/add', {type, contact});
    }

    refreshContactCode(type, contact) {
        return this.send('post', '/settings/contacts/refresh', {type, contact});
    }

    verifyContact(type, contact, code) {
        return this.send('post', '/settings/contacts/verify', {type, contact, code});
    }

    setMainContact(contact_id, branch_code) {
        return this.send('post', '/settings/contacts/main', {contact_id, branch_code});
    }

    setContactForSubscriptions(contact_id) {
        return this.send('post', '/settings/contacts/account', {contact_id});
    }

    removeContact(contact_id) {
        return this.send('post', '/settings/contacts/remove', {contact_id});
    }

    uploadAvatar(file, onUploadProgress = () => {
    }) {
        return this.send('post', '/settings/profile/avatar/upload', file, {'Content-Type': 'multipart/form-data'}, {onUploadProgress});
    }

    removeAvatar(files) {
        return this.send('post', '/settings/profile/avatar/remove', {files});
    }

    /**
     * ================================================================
     * Tickets
     * ================================================================
     */

    fetchTickets(account_id, limit, offset) {
        return this.send('get', '/ticket/list', {account_id, limit, offset});
    }

    ticketForm() {
        return this.send('get', '/ticket/form');
    }

    createTicket(fields) {
        return this.send('post', '/ticket/create', fields);
    }

    uploadTicketFiles(files, onUploadProgress = () => {
    }) {
        return this.send('post', '/ticket/files/upload', files, {'Content-Type': 'multipart/form-data'}, {onUploadProgress});
    }

    removeUploadedTicketFiles(files) {
        return this.send('post', '/ticket/files/remove', {files});
    }

    complainTicket(ticket_id, message, files) {
        return this.send('post', '/ticket/complain', {ticket_id, message, files});
    }

    complainTicketAnswer(ticket_id, message, files) {
        return this.send('post', '/ticket/complain-answer', {ticket_id, message, files});
    }

    evaluate(ticket_id, score) {
        return this.send('post', '/ticket/evaluate', {ticket_id, score});
    }

    /**
     * ================================================================
     * Entity API
     * ================================================================
     */

    getBranchesEntityList() {
        return this.send('get', '/branches/entity');
    }

    setEntityMainContact(contact_id) {
        return this.send('post', '/entity/settings/contacts/main', {contact_id});
    }

    getOrganizations() {
        return this.send('get', '/entity/organizations');
    }

    getEntityAccountBalance(organization_id, account_id) {
        return this.send('get', '/entity/organizations/balance', {
            organization_id,
            account_id,
        });
    }

    getPremises(organization_id, account_id, limit, offset) {
        return this.send('get', '/entity/premises', {
            organization_id,
            account_id,
            limit,
            offset,
        });
    }

    getEntityServices(organization_id, account_id) {
        return this.send('get', '/entity/services', {
            organization_id,
            account_id,
        });
    }

    entityIdentificationByOrganization(user_type, inn, kpp, account_number, branch_code, agreement) {
        return this.send('post', '/entity/identification', {
            user_type,
            inn,
            kpp,
            account_number,
            branch_code,
            agreement,
        });
    }

    entityIdentificationVerifyCode(code, agreement) {
        return this.send('post', '/entity/identification/verify', {
            code,
            agreement,
        });
    }

    /**
     * ================================================================
     * Entity Meter
     * ================================================================
     */

    getEntityMeterDeviceList(organization_id, account_id, premise_id, limit, offset) {
        return this.send('get', '/entity/meter/list', {
            organization_id,
            account_id,
            premise_id,
            limit,
            offset,
        });
    }

    sendEntityMeterDeviceMetrics(organization_id, account_id, premise_id, meter_id, t1, t2, t3, limit, offset) {
        return this.send('post', '/entity/meter/data/send', {
            organization_id,
            account_id,
            premise_id,
            meter_id,
            t1,
            t2,
            t3,
            limit,
            offset,
        });
    }

    getEntityPremiseAndMetersWithLastData(organization_id, account_id, premise_id, limit, offset) {
        return this.send(
            'get',
            '/entity/premise',
            {
                organization_id,
                account_id,
                premise_id,
                limit,
                offset,
            },
        );
    }

    getEntityPremiseWithoutMeters(organization_id, account_id, premise_id, limit, offset) {
        return this.send(
            'get',
            '/entity/premise/info',
            {
                organization_id,
                account_id,
                premise_id,
                limit,
                offset,
            }
        );
    }

    getEntityMetersWithLastData(organization_id, account_id, premise_id, limit, offset) {
        return this.send(
            'get',
            '/entity/meter/list/detail',
            {
                organization_id,
                account_id,
                premise_id,
                limit,
                offset,
            },
        );
    }

    getEntityMetersServices(organization_id, account_id, premise_id,) {
        return this.send('get', '/entity/meters/services', {
            organization_id,
            account_id,
            premise_id,
        });
    }

    /**
     * ================================================================
     * Entity Statistics
     * ================================================================
     */

    getEntityAccrualsStatistics(organization_id, account_id, period_from, period_to, limit, offset) {
        return this.send('get', '/entity/statistics/accruals', {
            organization_id,
            account_id,
            period_from,
            period_to,
            limit,
            offset,
        });
    }


    downloadEntityAccrualsStatistics(organization_id, account_id, account_number, period_from, period_to) {
        return this.send('get', '/entity/statistics/accruals/download', {
            organization_id,
            account_id,
            account_number,
            period_from,
            period_to,
        });
    }

    getEntityPaymentsStatistics(organization_id, account_id, period_from, period_to, limit, offset, source = null) {
        return this.send('get', '/entity/statistics/payments', {
            organization_id,
            account_id,
            period_from,
            period_to,
            limit,
            offset,
            source
        });
    }

    getEntityMetersData(organization_id, account_id, premise_id, period_from, period_to, limit, offset, service = null) {
        return this.send('get', '/entity/statistics/meters', {
            organization_id,
            account_id,
            premise_id,
            period_from,
            period_to,
            limit,
            offset,
            service,
        });
    }

    downloadEntityMetersStatistics(organization_id, account_id, account_number, premise_id, period_from, period_to) {
        return this.send('get', '/entity/statistics/meters/download', {
            organization_id,
            account_id,
            account_number,
            premise_id,
            period_from,
            period_to,
        });
    }

    getEntityRatesStatistics(organization_id, account_id, premise_id, service_id, period_from, period_to, limit, offset) {
        return this.send('get', '/entity/statistics/rates', {
            organization_id,
            account_id,
            premise_id,
            service_id,
            period_from,
            period_to,
            limit,
            offset,
        })
    }

    /**
     * ================================================================
     * Entity Tickets
     * ================================================================
     */

    createEntityTicket(organization_id, account_id, full_name, phone, email, category_code, subcategory_code, message, files = []) {
        return this.send(
            'post',
            '/entity/ticket/create',
            {
                organization_id,
                account_id,
                full_name,
                phone,
                email,
                category_code,
                subcategory_code,
                message,
                files,
            },
        );
    }

    /**
     * ================================================================
     * Entity Settings
     * ================================================================
     */


    profileEntity() {
        return this.send('get', '/entity/settings/profile');
    }

    saveEntityProfile(name, surname, patronymic, no_patronymic, timezone, branch_code, avatar, delete_file) {
        return this.send('post', '/entity/settings/profile', {
            name,
            surname,
            patronymic,
            no_patronymic,
            timezone,
            branch_code,
            avatar,
            delete_file,
        });
    }

    fetchEntityAccounts() {
        return this.send('get', '/entity/settings/accounts');
    }

    accountVisibility(organization_id, account_id, hidden) {
        return this.send('post', '/entity/settings/accounts/visibility', {
            organization_id,
            account_id,
            hidden,
        });
    }


    /**
     * ================================================================
     * Vendor API
     * ================================================================
     */

    createVendorTicket(account_id, email, category_code, subcategory_code, message, files = []) {
        return this.send(
            'post',
            '/vendor/ticket/create',
            {
                account_id,
                email,
                category_code,
                subcategory_code,
                message,
                files,
            },
        )
    }

    getBranchesVendorList() {
        return this.send('get', '/branches/vendor');
    }

    getVendorContracts() {
        return this.send('get', '/vendor/contracts');
    }

    getVendorAccrualsStatistics(
        contract_id,
        account_id,
        period_from = null,
        period_to = null,
        service_id = null,
        limit = null,
        offset = null,
    ) {
        return this.send(
            'get',
            '/vendor/statistics/accruals',
            {
                contract_id,
                account_id,
                period_from,
                period_to,
                service_id,
                limit,
                offset,
            }
        );
    }

    getVendorObjectInfoStatistics(contract_id, object_id, client_id) {
        return this.send(
            'get',
            '/vendor/statistics/object',
            {
                contract_id,
                object_id,
                client_id,
            }
        );
    }

    getVendorMetersStatistic(contract_id, account_id, service_id = null) {
        return this.send(
            'get',
            '/vendor/statistics/meters',
            {
                contract_id,
                account_id,
                service_id,
            }
        );
    }

    getVendorPaymentsStatistic(
        contract_id,
        account_id,
        period_from = null,
        period_to = null,
        service = null,
        limit = null,
        offset = null
    ) {
        return this.send(
            'get',
            '/vendor/statistics/payments',
            {
                contract_id,
                account_id,
                period_from,
                period_to,
                service,
                limit,
                offset,
            }
        );
    }

    /**
     * статистика показаний
     * @param vendor_id
     * @param contract_id
     * @param account_id
     * @param meter_id
     * @param service_id
     * @param period_from
     * @param period_to
     * @param limit
     * @param offset
     * @returns {Promise | Promise<unknown>}
     */
    getVendorMetersStatisticInfo(
        contract_id,
        account_id,
        service_id = null,
        period_from = null,
        period_to = null,
        limit = null,
        offset = null
    ) {
        return this.send(
            'get',
            '/vendor/statistics/meters-info',
            {
                contract_id,
                account_id,
                service_id,
                period_from,
                period_to,
                limit,
                offset,
            }
        );
    }

    getVendorReportLocalityList(
        needParamCode,
        contractId,
        reportCode,
        region = null,
        county = null,
        city = null,
        locality = null,
        street = null,
        house = null,
    ) {
        return this.send(
            'get',
            '/vendor/report/locality-list',
            {
                'need_param_code': needParamCode,
                'contract_id': contractId,
                'report_code': reportCode,
                'region': region,
                'county': county,
                'city': city,
                'locality': locality,
                'street': street,
                'house': house
            }
        )
    }

    getVendorPersonalAccount(
        contractId,
        accountNumber = null,
        region = null,
        county = null,
        city = null,
        locality = null,
        street = null,
        house = null,
        flat = null,
        korpus = null
    ) {
        return this.send(
            'get',
            '/vendor/personal-account',
            {
                'contract_id': contractId,
                'account_number': accountNumber,
                region,
                county,
                city,
                locality,
                street,
                house,
                flat,
                korpus
            }
        )
    }

    sendVendorReport(
        contractId,
        reportCode,
        region = null,
        county = null,
        city = null,
        locality = null,
        street = null,
        house = null,
        housing = null,
        flat = null,
        periodFrom = null,
        periodTo = null,
        service = null,
        period = null,
        categoryGroup = null,
        accountId = null,
        objectId = null
    ) {
        return this.send(
            'get',
            '/vendor/report/compose',
            {
                'contract_id': contractId,
                'report_type': reportCode,
                region,
                county,
                city,
                locality,
                street,
                house,
                housing,
                flat,
                'period_from': periodFrom,
                'period_to': periodTo,
                'service': service,
                'period': period,
                'category_group': categoryGroup,
                'account_id': accountId,
                'object_id': objectId
            }
        )
    }

    getVendorReceipt(
        contractId,
        accountId,
        period
    ) {
        return this.send(
            'get',
            '/vendor/receipts/download/',
            {
                'contract_id': contractId,
                'account_id': accountId,
                period
            }
        )
    }

    getVendorMetersWithLast(
        contract_id,
        account_id,
        limit = null,
        offset = null
    ) {
        return this.send(
            'get',
            '/vendor/meter/list',
            {
                contract_id,
                account_id,
                limit,
                offset,
            }
        );
    }

    sendVendorMeterDeviceMetrics(contractId, meterId, accountId, metrics) {
        return this.send('post', 'vendor/meter/send', {
            contract_id: contractId,
            meter_id: meterId,
            account_id: accountId,
            t1: metrics.t1,
            t2: metrics.t2,
            t3: metrics.t3,
        });
    }

    sendVendorMeterTemplate(formData){
        return this.send('post', 'vendor/meter/template/send', formData);
    }

    getVendorMeterTemplate(contract_id) {
        return this.send('get', 'vendor/meter/template/get', {
            contract_id
        });
    }

    getVendorConsumptionStatistics(
        contract_id,
        account_id,
        period_from,
        period_to,
        service = null,
        limit = null,
        offset = null,
    ) {
        return this.send(
            'get',
            '/vendor/statistics/consumptions',
            {
                contract_id,
                account_id,
                period_from,
                period_to,
                service,
                limit,
                offset,
            }
        );
    }

    /**
     * ================================================================
     * Authorization | Registration
     * ================================================================
     */

    login(credentials) {
        return this.send('post', '/auth/login', credentials);
    }

    loginEntity(login, password, login_type, device_type = 'mobile') {
        return this.send('post', '/auth/login/entity', {
            login,
            password,
            login_type,
            device_type,
        });
    }

    getLoginFormText(text_type) {
        return this.send('get', '/auth/entity/login/text', {
            text_type,
        });
    }

    loginVendor(login, password, device_type = 'mobile') {
        return this.send('post', '/auth/login/vendor', {
            login,
            password,
            device_type,
        });
    }

    socialAuthWithAccessToken(social, code, os) {
        return this.send('post', '/auth/social', {social, code, os});
    }

    logout() {
        return this.send('post', '/auth/logout');
    }

    refreshToken(refresh_token) {
        return this.send('post', '/auth/refresh', {refresh_token});
    }

    register(data) {
        return this.send('post', '/register', data);
    }

    registerEntity(surname, name, patronymic, no_patronymic, branch_code, email, phone, password, password_confirmation, consent, sms_code, email_code) {
        return this.send('post', '/register/entity', {
            surname,
            name,
            patronymic,
            no_patronymic,
            branch_code,
            email,
            phone,
            password,
            password_confirmation,
            consent,
            sms_code,
            email_code,
        });
    }

    registerSimple(email, token, phone, sms_code, password, password_confirmation) {
        return this.send('post', '/register/entity/simple', {
            email,
            token,
            phone,
            sms_code,
            password,
            password_confirmation,
        });
    }

    verifyEntityCode(email, token) {
        return this.send('post', '/register/entity/simple/verify', {
            email,
            token
        });
    }

    refreshRegisterContactCode(type, contact) {
        return this.send('post', '/register/contacts/refresh', {type, contact});
    }

    verifyRegisterContact(type, contact, code) {
        return this.send('post', '/register/contacts/verify', {type, contact, code});
    }

    resetPassword(data) {
        return this.send('post', '/password/reset', data);
    }

    resetEntityPassword(contact, code, password, password_confirmation) {
        return this.send('post', '/password/reset/entity', {
            contact,
            code,
            password,
            password_confirmation,
        });
    }

    resetPasswordSendCode(contact, branch_code) {
        return this.send('post', '/password/restore', {
            contact,
            branch_code,
        });
    }

    resetEntityPasswordSendCode(contact) {
        return this.send('post', '/password/restore/entity', {
            contact,
        });
    }

    refreshRestorePasswordContactCode(contact) {
        return this.send('post', '/password/refresh', {contact});
    }

    verifyRestorePasswordContact(contact, code) {
        return this.send('post', '/password/verify', {contact, code});
    }

    changePassword(oldPassword, newPassword, newPasswordConfirmation) {
        return this.send('post', '/settings/password/change', {
            old_password: oldPassword,
            new_password: newPassword,
            new_password_confirmation: newPasswordConfirmation,
        });
    }

    sendVerifyOldLkPhoneCode(phone) {
        return this.send('post', '/auth/old-lk/send-code', {phone});
    }

    refreshVerifyOldLkPhoneCode(phone) {
        return this.send('post', '/auth/old-lk/refresh-code', {phone});
    }

    verifyOldLkPhoneData(payload) {
        return this.send('post', '/auth/old-lk/verify', payload);
    }

    entityVerifyOldLkPhoneData(payload) {
        return this.send('post', '/auth/old-lk/entity-verify', payload);
    }

    getTextPageContent(code) {
        return this.send('get', '/page', {code});
    }
}

var esApi = new EsApi();

export default esApi;

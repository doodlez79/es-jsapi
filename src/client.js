import axios from 'axios';

export default class Client {
  static _client = null;

  static configure(defaults) {
    Client._client = axios.create(defaults);
  }

  static setEndpoint(url) {
    Client._endpoint = url;
  }

  static getClient() {
    return this._client;
  }

  static getObjectsList() {
    return Client.getClient().get('object/list');
  }
}
